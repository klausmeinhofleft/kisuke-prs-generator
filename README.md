# Pandoc Reveal.js Slides Generator

Easy slides generator for lazy people!

## Requirements:

-  [pandoc](https://pandoc.org/) of course
-  [bash](https://www.gnu.org/software/bash/)) maybe (?)
-  refill [slides.md](./slides.md) with stuff ( Yes, you must to work. Unfortunately I can't do all for you :D )

## How To Use

```bash
# ./make_slides.sh ${input_file} ${output_file} ${reveal.js_theme}
./make_slides.sh slides.md slides moon
# this create a output file named slides.html
```

## ToDo

- [ ] Documentation
    - [ ] enhancement of documentation
- [ ] enhancement of [make_slides.sh](./make_slides.sh) (support for parameters)
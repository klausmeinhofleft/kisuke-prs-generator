#!/usr/bin/env bash
/usr/bin/env pandoc --verbose -t revealjs -V theme="${3}" --standalone "${1}" -o "${2}.html"
---
author: Jhon Doe 
title: Super Title!
lang: en
date: 2019-03-05
---

<!-- to create vertical slides leave empty this slide and make a subtitle in markdown -->
# Horizontal Slide

## Vertical slide
<!-- The way to make vertical slide -->

  Some Text

<!-- other way to make a vertical slide -->
---

### titulo

#### h4(?)

normal text

_cursive text_

__bold text__

---
#### list:

  -  item 1
  -  item 2

  - [ ] checkbox
    - [x] filled checkbox

<!-- return to horizontal slides -->
# titulo2

## Code

```bash
echo "hello world"
```